var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var fullTime = 10000
function timeDown(x){
    if (x==books.length){
        return 0
    }
    readBooksPromise(fullTime,books[x])
        .then(function (promise){
            fullTime=promise
            timeDown(x+1)
        })
        .catch(function (error){
            console.log(error.message);
        })
        
}
timeDown(0)
