/**
 * Soal No. 1 (Range)
 */
console.log("\x1b[36m%s\x1b[0m", "Soal No. 1 (Range)\n");

function range(startNum, finishNum) {
	const VALUE_OF_FAIL = -1;

	if (!startNum || !finishNum) {
		return VALUE_OF_FAIL;
	}

	if (isNaN(startNum) || isNaN(finishNum)) {
		return VALUE_OF_FAIL;
	}

	const results = [];
	let startAt = Number(startNum);
	let finishAt = Number(finishNum);

	if (startAt > finishAt) {
		for (let i = startAt; i >= finishAt; i--) {
			results.push(i);
		}
	} else {
		for (let i = startAt; i <= finishAt; i++) {
			results.push(i);
		}
	}

	return results;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());
console.log(range("30", "36"));

/**
 * Soal No. 2 (Range with Step)
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 2 (Range with Step)\n");

function rangeWithStep(startNum, finishNum, step) {
	const VALUE_OF_FAIL = -1;

	if (!startNum || !finishNum || !step) {
		return VALUE_OF_FAIL;
	}

	if (isNaN(startNum) || isNaN(finishNum) || isNaN(step)) {
		return VALUE_OF_FAIL;
	}

	const results = [];
	let startAt = Number(startNum);
	let finishAt = Number(finishNum);
	const everyStep = Number(step);

	if (startAt > finishAt) {
		for (let i = startAt; i >= finishAt; ) {
			results.push(i);
			i = i - everyStep;
		}
	} else {
		for (let i = startAt; i <= finishAt; ) {
			results.push(i);
			i = i + everyStep;
		}
	}

	return results;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log(rangeWithStep(6, 2));
console.log(rangeWithStep());
console.log(rangeWithStep("3", "12", "3"));

/**
 * Soal No. 3 (Sum of Range)
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 3 (Sum of Range)\n");

function sum(startNum, finishNum, step = 1) {
	if (!finishNum) {
		finishNum = startNum;
	}

	const numbers = rangeWithStep(
		Number(startNum),
		Number(finishNum),
		Number(step)
	);
	let total = 0;

	for (let i = 0; i < numbers.length; i++) {
		total += numbers[i];
	}

	return total;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log(sum("3", "5"));

/**
 *
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 4 (Array Multidimensi)\n");

function dataHandling(data) {
	for (let i = 0; i < data.length; i++) {
		console.log(`Nomor ID: ${data[i][0]}`);
		console.log(`Nama Lengkap: ${data[i][1]}`);
		console.log(`TTL: ${data[i][2]} ${data[i][3]}`);
		console.log(`Hobi: ${data[i][4]}\n`);
	}
}

let input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling(input);

/**
 *
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 5 (Balik Kata)\n");

function balikKata(originalString) {
	let reverseString = "";

	for (let i = originalString.length - 1; i >= 0; i--) {
		reverseString += originalString[i];
	}

	return reverseString;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

/**
 *
 */
console.log("\x1b[36m%s\x1b[0m", "\nSoal No. 6 (Metode Array)\n");

function dataHandling2(input) {
	// Case 1
	let output1 = input;
	output1.splice(
		1,
		4,
		"Roman Alamsyah Elsharawy",
		"Provinsi Bandar Lampung",
		"21/05/1989",
		"Pria",
		"SMA Internasional Metro"
	);
	console.log(output1);

	// Case 2
	let date1 = input[3];
	let dateArray = date1.split("/");
	let monthName = "";

	switch (dateArray[1]) {
		case "01":
			monthName = "Januari";
			break;
		case "02":
			monthName = "Februari";
			break;
		case "03":
			monthName = "Maret";
			break;
		case "04":
			monthName = "April";
			break;
		case "05":
			monthName = "Mei";
			break;
		case "06":
			monthName = "Juni";
			break;
		case "07":
			monthName = "Juli";
			break;
		case "08":
			monthName = "Agustus";
			break;
		case "09":
			monthName = "September";
			break;
		case "10":
			monthName = "Oktober";
			break;
		case "11":
			monthName = "November";
			break;
		default:
			monthName = "Desember";
	}

	console.log(monthName);

	// Case 3
	let newDateArray = [...dateArray];
	newDateArray.sort(function (first, second) {
		let a = Number(first);
		let b = Number(second);

		if (a < b) {
			return 1;
		}

		if (a > b) {
			return -1;
		}

		return 0;
	});
	console.log(newDateArray);

	// Case 4
	console.log(dateArray.join("-"));

	// Case 5
	console.log(output1[1].slice(0, 15));
}

let input2 = [
	"0001",
	"Roman Alamsyah ",
	"Bandar Lampung",
	"21/05/1989",
	"Membaca",
];
dataHandling2(input2)