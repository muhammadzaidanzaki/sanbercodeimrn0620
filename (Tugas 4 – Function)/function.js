// No. 1
function teriak() {
	return 'Halo Sanbers!';
}

console.log(teriak());

console.log('\n');


// No. 2
function kalikan() {
	return num1 * num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log('\n');


// No. 3
function introduce() {
	return 'Nama saya ' + name + ', Umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!';
}

var name = 'Agus';
var age = 30;
var address = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

var perkenalkan = introduce(name, age, address, hobby);
console.log(perkenalkan)
