function bandingkan(num1, num2) {
    if (!num2) {
      if (!num1) { // tidak ada parameter
        return -1
      } else { // cuma 1 parameter
        if (num1 > 0) {
          return num1
        } else {
          return -1
        }
      }
    }
    
    if (num1 < 0 || num2 < 0) {
      return -1
    }
  
    if (num1 == num2) {
      return -1
    }
  
    // num1 dan num2 exist, positif, dan tidak sama
    if (num1 > num2) {
      return num1
    } else {
      return num2
    }
  }
  
  function balikString(kata) {
    const length = kata.length
    let result = ''
    for (var i = 0; i < length; i++) {
        let char = kata[length-i-1]
        result += char
    }
  
    return result
  }
  
  function palindrome(kata) {
    if (kata === balikString(kata)) {
      return true
    }
    return false
  }
  
  // TEST CASES Bandingkan Angka
  console.log(bandingkan(10, 15)); // 15
  console.log(bandingkan(12, 12)); // -1
  console.log(bandingkan(-1, 10)); // -1 
  console.log(bandingkan(112, 121));// 121
  console.log(bandingkan(1)); // 1
  console.log(bandingkan()); // -1
  console.log(bandingkan("15", "18")) // 18
  
  // TEST CASES BalikString
  console.log(balikString("abcde")) // edcba
  console.log(balikString("rusak")) // kasur
  console.log(balikString("racecar")) // racecar
  console.log(balikString("haji")) // ijah
  
  // TEST CASES Palindrome
  console.log(palindrome("kasur rusak")) // true
  console.log(palindrome("haji ijah")) // true
  console.log(palindrome("nabasan")) // false
  console.log(palindrome("nababan")) // true
  console.log(palindrome("jakarta")) // false
  
//   *nb = mohon maaf ini saya mengerjakan dengan berdiskusi dengan teman saya karena stuck