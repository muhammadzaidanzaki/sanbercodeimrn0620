
function DescendingTen(num) {
if (!num) {
  return -1
}

let result = ''
for (let i = num; i > num-10; i--) {
  result += `${i} `
}  
return result
}

function AscendingTen(num) {
if (!num) {
  return -1
}

let result = ''
for (let i = num; i < num+10; i++) {
  result += `${i} `
}  
return result
}

function ConditionalAscDesc(reference, check) {  
if (!reference || !check) {
  return -1
}

if (check % 2 === 1) {
  return AscendingTen(reference)
} else {
  return DescendingTen(reference)
}
}

function ularTangga() {
return `${DescendingTen(100)}
${AscendingTen(81)}
${DescendingTen(80)}
${AscendingTen(61)}
${DescendingTen(60)}
${AscendingTen(41)}
${DescendingTen(40)}
${AscendingTen(21)}
${DescendingTen(20)}
${AscendingTen(1)}
`
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// TEST CASE Ular Tangga
console.log(ularTangga())
/*
Output :
100 99 98 97 96 95 94 93 92 91
81 82 83 84 85 86 87 88 89 90
80 79 78 77 76 75 74 73 72 71
61 62 63 64 65 66 67 68 69 70
60 59 58 57 56 55 54 53 52 51
41 42 43 44 45 46 47 48 49 50
40 39 38 37 36 35 34 33 32 31
21 22 23 24 25 26 27 28 29 30
20 19 18 17 16 15 14 13 12 11
1 2 3 4 5 6 7 8 9 10
*/
