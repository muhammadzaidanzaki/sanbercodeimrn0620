//Soal No. 1 Animal class

class Animal {
    legs = 4;
    cold_blooded = false;
    constructor(name){
        this._name = name;
    }
    get name(){
        return this._name;
    }
    set name(x){
        this._name =x;
    }
}

console.log('Soal No. 1');
//release 0
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


//release 1
class Ape extends Animal {
    constructor(name){
        super(name);
    }
    yell(){
        console.log('Auooo');
    }
}
 
class Frog extends Animal {
    constructor(name){
        super(name);
    }
    jump(){
        console.log('Hop hop');
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


//soal No. 2

class Clock {
    constructor({template}){
        this._template = template;
        this.timer;
    }

    render(){
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this._template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }

    get stop(){
        clearInterval(this.timer);
    };
    
    get start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    };

} 

console.log('\n\nSoal No. 2');
var clock = new Clock({template: 'h:m:s'});
clock.start;  
